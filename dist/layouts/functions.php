<?php 

function createNewsTable( $db_connect ) {
  mysqli_query($db_connect, "CREATE TABLE IF NOT EXISTS `news` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `rating` float NOT NULL,
    `count_votes` int(10) NOT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
}

function createVotesTable( $db_connect ) {
  mysqli_query($db_connect, "CREATE TABLE IF NOT EXISTS `votes_news2user` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `user_id` varchar(255) NOT NULL,
    `news_id` int(10) NOT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
}
?>