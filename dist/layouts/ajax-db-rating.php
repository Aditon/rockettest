<?php

// DB connection
require_once( 'db_connection.php' );
require_once( 'functions.php' );

// Error container 
$error = false;
// Get user data
$user_rating = isset($_GET['rating']) ? (int) $_GET['rating'] : null;
$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : null;
$news_id = 1;

// Create news table if not exists
createNewsTable( $db_connect );

// Write to news table
mysqli_query($db_connect, "INSERT INTO `news` (`id`, `title`, `rating`, `count_votes`) VALUES (1, 'Новость 1', 0, 0)");

// Create votes table if not exists
createVotesTable( $db_connect );

// Check whether the user voted earlier
$sql = mysqli_query($db_connect, "SELECT count(*) FROM `votes_news2user` WHERE `user_id`={$user_id} AND `news_id`={$news_id}") or die(mysqli_error($db_connect));


$result = mysqli_fetch_row($sql);


if($result[0] > 0){
  $error = true;
}else{
// if the user did not vote, we will vote
// make a write that the user voted
  mysqli_query($db_connect, "INSERT INTO `votes_news2user` (`user_id`, `news_id`) VALUES ({$user_id}, {$news_id})") or die(mysqli_error()); 
  // make a write for the news-increase the number of votes and the number of voters
  mysqli_query($db_connect, "
    UPDATE `news` SET `rating` = (`rating` * `count_votes` + $user_rating) / (`count_votes` + 1), `count_votes` = `count_votes` + 1 WHERE `id` = {$news_id}") or die(mysqli_error());
}

$feedback = array(
  'result' => 'error',
  'rating' => $user_rating,
  'msg' => $error,
  'user_id' => $user_id
);

// Feedback for client
if($error){
    echo json_encode($feedback);
}else{
    $feedback['result'] = 'success';
    echo json_encode($feedback);
}

?>