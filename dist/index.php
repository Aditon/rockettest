<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Test</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>

    <?php require_once( 'layouts/db_connection.php' ); ?>

    <?php
      $user = false;
      $user_id = null;
      $user_data = null;
      $user_rating = 0;

      if( isset($_COOKIE['testUserID']) ) {
        $user_id = $_COOKIE['testUserID'];
        $user = true;
      }
      if( isset($_COOKIE['testUserRating']) ) {
        $user_rating = $_COOKIE['testUserRating'];
      }


      // Get post table
      $sql_post = mysqli_query($db_connect, "SELECT * FROM `news`") or die(mysqli_error($db_connect));
      $sql_users = mysqli_query($db_connect, "SELECT * FROM `votes_news2user` WHERE `user_id` IS NOT null ") or die(mysqli_error($db_connect));

      $users_count = mysqli_num_rows($sql_users);
      while($r = mysqli_fetch_assoc($sql_post)){
          $rating_data = $r;
      }
      
    ?>

    <main>
      <div class="container">
        <!-- Rating box -->
        <div class="rating-box">

            <div class="rating">
                <div class="rating-stars watcher js-ratingStarsBox <?php if( !$user_id ) {echo 'watcher';} ?>">

                  <div class="row">
                  <?php 
                    $total_stars = 5;
                    $aria_label = ['Ужасно', 'Плохо', 'Нормально', 'Хорошо', 'Отлично'];

                    if( !$user ) {

                      $i = $total_stars;

                      while ( $i >= 1 ) { ?>

                        <input type="radio" name="rating" id="mark<?php echo $i; ?>" class="hidden">
                        <label for="mark<?php echo $i; ?>" aria-label="<?php echo $aria_label[ $i - 1 ]; ?>">
                          <span class="rating-star-icon ajax-rating js-ratingStar" data-rating="<?php echo $i; ?>"></span>
                        </label>

                      <?php $i--; }
                    }else {

                      $j = $total_stars;

                      while( $j >= 1 ) { ?>
                        <span class="rating-star-icon <?php if( $j == $user_rating ) { echo 'checked-star'; } ?>" aria-label="<?php echo $aria_label[ $j - 1 ]; ?>" data-rating="<?php echo $j; ?>"></span>

                      <?php $j--;}
                      }
                    ?>
                  </div>
                </div>
                <?php
                  $rating_total = $rating_data['rating'];
                ?>
                <div class="rating-sum js-ratingSum">Кол-во голосов: <b class="count"><?php echo $users_count; ?></b></div>
            </div>
            <span class="ratings-message js-ratingMessage" data-total="<?php echo $rating_total ?>">Средняя оценка: <b class="count"><?php echo round($rating_total, 1); ?></b></span>
        </div>

        <!-- Progress bar -->
        <div class="progress-bar-box">
          <div class="progress-bar js-progressBar" data-progress="1">
            <span class="progress-bar__line">1%</span>
          </div>

          <select name="progress-bar" id="progress-bar-value">
            <option selected disabled>Выберите значение</option>
            <option value="8">8</option>
            <option value="16">16</option>
            <option value="32">32</option>
            <option value="64">64</option>
          </select>

          <button class="js-progressBarSubmit">On</button>
        </div>

      </div>
    </main>

    <script src="js/script.js"></script>
  </body>
</html>