"use strict";

const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const less = require('gulp-less');
const postcss = require('gulp-postcss');
const postcssImport = require('postcss-import');
const postcssCssnext = require('postcss-cssnext');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const autoprefixer = require('autoprefixer');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync');

const del = require('del');

// for image optimization
const image = require('gulp-image');

gulp.task('browser-sync', function(done) {
  browserSync.init({
    proxy: "frontend-less-nobs.dev/dist/", //domain in Open Server
    notify: false
  });
  done();
});

function reload(done){
  browserSync.reload();
  done();
}

gulp.task("less", function(done){
  let processors = [
    postcssImport,
    postcssCssnext,
    autoprefixer
  ];
  return gulp.src([
      'src/css/*.less',
    ])
    .pipe(less())
    .pipe(sourcemaps.init())
    .pipe(postcss(processors))
    .on('error', function (err) {
          console.log(err.toString());
          this.emit('end');
      })
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write('/'))
    .pipe(gulp.dest('./dist/css'))
  done();
});

// gulp.task('css', function(done) {
//   gulp.src('src/css/*.css')
//     .pipe(gulp.dest('dist/css/'));
//   done();
// });

gulp.task('fonts', function(done) {
  gulp.src('src/fonts/**/*')
    .pipe(gulp.dest('dist/fonts/'));
  done();
});

gulp.task('img',function(done){
  return gulp.src(['src/img/**/*'])
  .pipe(gulp.dest('./dist/img'));
});

gulp.task('js', function(done) {
  gulp.src('src/js/*.js')
    .pipe(concat('script.js'))
    .pipe(uglify({toplevel: true}))
    .pipe(gulp.dest('dist/js/'));
  done();
});

gulp.task('watch', function(done) {
  gulp.watch('src/css/*.less', gulp.series('less', reload));
  // gulp.watch('src/css/*.css', gulp.series('css', reload));
  gulp.watch(['src/img/*'], gulp.series('img', reload));
  gulp.watch('src/fonts/*', gulp.series('fonts', reload));
  gulp.watch('src/js/*.js', gulp.series('js', reload));
  gulp.watch('dist/*.php',reload);
  done();
});

gulp.task('imagemin', function(done) {
  gulp.src('src/img/*')
    .pipe(image())
    .pipe(gulp.dest('dist/img'))
  done();
});


gulp.task('del', function(done) {
  (async () => {
    const deconstedPaths = await del(['dist/img/', 'dist/css/', 'dist/js/', '!build']);
     console.log('Deconsted files and folders:\n', deconstedPaths.join('\n'));
})();
  done();
});



gulp.task('default', gulp.series('img', 'fonts', 'less', 'js', 'browser-sync', 'watch'));
gulp.task('build', gulp.series('img', 'fonts', 'less', 'js', 'imagemin'));