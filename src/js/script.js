"use strict";

let stars = document.querySelectorAll( '.js-ratingStar' ),
    ratingContainer = document.querySelector( '.js-ratingStarsBox' ),
    rating = 0;

// Ajax get query
function setQuery( userName, ratingValue ) {

  let user_id = userName;
  let rating = ratingValue;

  const request = new XMLHttpRequest();

  const url = "layouts/ajax-db-rating.php?user_id=" + user_id + "&rating=" + rating;

  request.open('GET', url);

  request.setRequestHeader('Content-Type', 'application/x-www-form-url');
   
  request.addEventListener("readystatechange", () => {
    if (request.readyState === 4 && request.status === 200) {
      let requestResult = JSON.parse(request.responseText) || false;

      if( requestResult instanceof Object && requestResult.constructor === Object ) {
        console.log( requestResult );
      }else {
        console.log( 'Something went wrong =( ', requestResult );
      }
      return request.responseText;
    }else {
      return 'Error!';
    }
  });
   
  // Get query
  request.send();
}

// User cookie method
let userCookie = {
  userName: 'testUserID',
  userRating: 'testUserRating',
  userRatingTotal: 0,


  getCookie: function( val ) {
    let result = document.cookie.match( '(^|;) ?' + val + '=([^;]*)(;|$)' );
    return result ? result : false;
  },

  setCookie: function( rating ) {
    let cookieName = this.setCookieName(),
        daysToAdd = 14,
        date = new Date(Date.now() + 86400e3);
        date = date.toUTCString();

    document.cookie = ( cookieName + '; expires=' + date );
    document.cookie = ( this.userRating + '=' + rating + '; expires=' + date );

    return parseInt(cookieName.replace(/[^\d]/g, ''));
  },

  setCookieName: function() {
    return this.userName + '=' + Math.floor( Math.random() * 100 );
  },

  checkCookie: function( num ) {
    let ratingNum = this.userRatingTotal;

    if( this.getCookie( this.userName ) ){
      return false;
    }else {
      return this.setCookie( ratingNum );
    }
  },
}

function recountRating( rating ) {
  let ratingSum = document.querySelector( '.js-ratingSum .count' ),
      ratingMessage = document.querySelector( '.js-ratingMessage' ),
      ratingMessageCounter = ratingMessage.querySelector('.count'),
      ratingMessageTotal = Number(ratingMessage.dataset.total) || 0,
      newRating,
      countVotest;

  countVotest = ratingSum.innerText = Number(ratingSum.innerText) + 1;
  newRating = ( (ratingMessageTotal * countVotest) + rating ) / (countVotest + 1);
  ratingMessageCounter.innerText = (newRating).toPrecision(2);
}

// Stars events
stars.forEach( function( item ) {
  item.onclick = function() {
    rating = Number(this.dataset.rating);

    checkInput();

    // Blocks parent wrapper if class watcher is missing 
    ratingContainer.classList.remove( 'watcher' );

    userCookie.userRatingTotal = rating;

    let cookieResult = userCookie.checkCookie();

    if( cookieResult ) {
      setQuery( cookieResult, rating );
      recountRating( rating );
    }
  }
} );

function checkInput() {
  if( !ratingContainer.classList.contains( 'watcher' ) ) {
    ratingContainer.addEventListener( 'click', function(e) {
      e.preventDefault();
      return false;
    } );
  }
}

// Progress bar
let progressBarSubmit = document.querySelector( '.js-progressBarSubmit' );

let progressBar = {
  progressBarElem: document.querySelector( '.js-progressBar' ),
  progressBarValue: document.getElementById( 'progress-bar-value' ),
  progressBarLine: document.querySelector( '.progress-bar__line' ),

  
  getTotalValue: function() {
    return Number(this.progressBarValue.options[this.progressBarValue.options.length - 1].value);
  },

  getProgressValue: function() {
    return Number(this.progressBarValue.value) || 0;
  },

  getPercent: function() {
    let current = this.getProgressValue(),
        total = this.getTotalValue();
    return current ? ( parseFloat( ( (100 * ( current / total ) * 100)/100 ) ) ) : false;
  },

  getResult: function() {
    let totalValue = this.getPercent(),
        res = totalValue + '%';

    if( totalValue && totalValue != Number( this.progressBarElem.dataset.progress ) ) {
      this.progressBarElem.dataset.progress = totalValue;
      this.progressBarLine.style.width = res;
      this.progressBarLine.innerHTML = res;
    }
  },
}

progressBarSubmit.onclick = function() {
  progressBar.getResult( );
}